module.exports = {
    aws_remote: {
        accessKeyId: '',
        secretAccessKey: '',
        region: 'us-east-1'
    },
    table_name: {
        latestRecognisedEmployeeTableName: 'last-recognised-employee'
    }
};