var AWS = require("aws-sdk");
var configurations = require('../configurations');
AWS.config.update(configurations.aws_remote);

var docClient = new AWS.DynamoDB.DocumentClient();
var table = configurations.table_name.latestRecognisedEmployeeTableName;

var params = {
    TableName: table
};

exports.getLatestEmployee = function () {
    let employee = null;
    let promise = docClient.scan(params).promise();

    function onSucess(data){
        console.log(data);
    };

    function onError(error){
        console.error(error);
    };

    promise.then(onSucess, onError);

    return promise;
};