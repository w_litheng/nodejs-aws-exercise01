var dynamodbAdapter = require('adapters/dynamodbAdapter');

exports.get_latest_employee = function() {

    var promise = dynamodbAdapter.getLatestEmployee()
    .then(function(result){
        console.log('Sucessfully retrieve latest employee = ', result);
        let uniqueId = result.Items[0].UniqueId;
        let fullName = result.Items[0].FullName;
        let position = result.Items[0].personPosition;
        let contact = result.Items[0].ContactNumber;
        let employeeId = result.Items[0].EmployeeId;
        let imageName = result.Items[0].ImageName;

        let employee = {
            uniqueId: uniqueId,
            fullName: fullName,
            position: position,
            contact: contact,
            employeeId: employeeId,
            imageName: imageName
        };
        
        return employee;
    });
    
    return promise;
};