var AWS = require("aws-sdk");
var configurations = require('../configurations');
AWS.config.update(configurations.aws_remote);

var docClient = new AWS.DynamoDB.DocumentClient();
var table = configurations.table_name.latestRecognisedEmployeeTableName;

var uniqueId = 1;
var fullname = "howChinBoon";

var params = {
    TableName: table
};

exports.getLatestEmployee = function () {
    console.log('Configurations = ', configurations);

    let employee = null;
    let promise = docClient.scan(params).promise();

    function onSucess(data){
        console.log('success = ', data);
    };

    function onError(error){
        console.log('error = ', error);
    };

    promise.then(onSucess, onError);

    return promise;
};