var controller = require('employeeController.js');
exports.handler = async (event) => {
    var promise = controller.get_latest_employee()
    .then(function(employee){
            console.log('employee = ', employee);
            const response = {
            statusCode: 200,
            body: JSON.stringify(employee),
            headers: {
                "Access-Control-Allow-Origin": "*"
            }
        };
        
        return response;
    });
    
    return promise;
};
