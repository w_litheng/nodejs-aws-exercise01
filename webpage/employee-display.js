let currentEmployee = null;
let pollingInterval = 5000; // Default 5000 miliseconds
let url = "http://localhost:8080/employee";
let xhttp = new XMLHttpRequest();

setInterval(getLatestEmployee, pollingInterval);

function getLatestEmployee(){
  console.log("Get latest employee.")

  function updateUIElements(){
    let fullNameElement = document.getElementById('fullName');
    let positionElement = document.getElementById('position');
    let contactElement = document.getElementById('contact');
    let appreciationQuote = document.getElementById('appreciationQuote');
    let imageElement = document.getElementById('imageName');
    
    console.log("Updating latest employee.")
    // update the UI elements
    fullNameElement.innerHTML = currentEmployee.fullName;
    positionElement.innerHTML = currentEmployee.position;
    contactElement.innerHTML = currentEmployee.contact;
    appreciationQuote.innerHTML = '#GoodJob' + currentEmployee.fullName.replace(/ /g,'');
    imageElement.src = "img/" + currentEmployee.imageName;
  };

  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var latestEmployee = JSON.parse(this.response);

      if(!currentEmployee
          || currentEmployee.employeeId != latestEmployee.employeeId) {
        currentEmployee = latestEmployee;
        updateUIElements();
      };
    }
  }

  xhttp.open("GET", url, true);
  xhttp.send();
};