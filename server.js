var express = require('express'),
  app = express(),
  port = process.env.PORT || 8080;

app.listen(port);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var routes = require('./api/routes/employeeRoute'); //importing route
routes(app); //register the route

console.log('Get latest employee REST API server started on: ' + port);