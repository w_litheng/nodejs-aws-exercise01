module.exports = function(app) {
  var employee = require('../controllers/employeeController');

  // employee Routes
  app.route('/employee')
    .get(employee.get_latest_employee);
};